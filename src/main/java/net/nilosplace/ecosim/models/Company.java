package net.nilosplace.ecosim.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

import lombok.Data;
import lombok.ToString;
import net.nilosplace.ecosim.App;

@Data @ToString(exclude = {"bank"})
public class Company extends BaseEntity {
	
	private Integer minSkillLevel;
	private Integer maxSkillLevel;
	
	private List<Person> employees = new ArrayList<>();

	// Assets
	private Double cash = 100000.0;
	// Liabilities
	private List<Loan> loans;
	
	private Integer maxEmployees;
	
	private Bank bank;
	
	public Company(Bank bank) {
		this.bank = bank;

		int t1 = App.random.nextInt(20);
		int t2 = App.random.nextInt(20);
		
		minSkillLevel = Math.min(t1, t2);
		maxSkillLevel = Math.max(t1, t2);
		
		if(minSkillLevel == 0) {
			maxEmployees = 200;
		} else {
			NormalDistribution dist = new NormalDistribution((int)(200 - ((40 * minSkillLevel) / Math.sqrt(minSkillLevel))), 20);
			maxEmployees = (int)dist.inverseCumulativeProbability(App.random.nextDouble());
			if(maxEmployees <= 2) {
				maxEmployees = 3;
			}
		}

		bank.addDeposit(id, cash);
	}

	public boolean addEmployee(Person person) {
		if(employees.size() < maxEmployees && person.getSkillLevel() >= minSkillLevel && person.getSkillLevel() <= maxSkillLevel) {
			person.setCompany(this);
			employees.add(person);
			return true;
		}
		return false;
	}
}
