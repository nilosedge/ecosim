package net.nilosplace.ecosim.models;

import java.util.List;

import lombok.Data;

@Data
public class CentralBank {
	private List<Reserve> reserves;
}
