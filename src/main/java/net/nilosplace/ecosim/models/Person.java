package net.nilosplace.ecosim.models;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import net.nilosplace.ecosim.App;
import net.nilosplace.ecosim.enums.SpendingType;

@Data @ToString(callSuper = true, exclude = {"bank"})
public class Person extends BaseEntity {
	private Salary salary;
	private Double expenses;
	private List<Loan> loans;
	private SpendingType spendingType;

	private Integer yearsOfExperiance;
	private Integer skillLevel = App.random.nextInt(7);
	
	private Bank bank;
	private Company company;

	public Person(Bank bank) {
		this.bank = bank;
		double rand = App.random.nextDouble();
		
		salary = new Salary();
		if(rand > 0.965) { salary.setValue(102100); skillLevel = 19; }
		else if(rand > 0.929) { salary.setValue(86975); skillLevel = 18; }
		else if(rand > 0.893) { salary.setValue(71851); skillLevel = 17; }
		else if(rand > 0.857) { salary.setValue(66432); skillLevel = 16; }
		else if(rand > 0.622) { salary.setValue(60705); skillLevel = 15; }
		else if(rand > 0.572) { salary.setValue(51548); skillLevel = 14; }
		else if(rand > 0.418) { salary.setValue(42391); skillLevel = 13; }
		else if(rand > 0.368) { salary.setValue(39362); skillLevel = 12; }
		else if(rand > 0.089) { salary.setValue(35540); skillLevel = 11; }
		else if(rand > 0.080) { salary.setValue(26557); skillLevel = 10; }
		else if(rand > 0.072) { salary.setValue(26092); skillLevel = 9; }
		else if(rand > 0.064) { salary.setValue(25627); skillLevel = 8; }
		else { salary.setValue(25162); skillLevel = App.random.nextInt(8); }
		
		spendingType = SpendingType.getRanomSpendingType();
		
		yearsOfExperiance = App.random.nextInt(20);
		
		expenses = (App.random.nextDouble() * 0.20) + 0.1;

		bank.addDeposit(id, salary.getAmount() / 10);
		
		/*
		8.9% had less than a high school diploma or equivalent.
		27.9% had high school graduate as their highest level of school completed. 
		14.9% had completed some college but not a degree.
		10.5% had an associate degree as their highest level of school completed.
		23.5% had a bachelor’s degree as their highest degree.
		14.4% had completed an advanced degree such as a master’s degree, professional degree or doctoral degree.
		*/

		// Education
		/*
		rand 
		08 25162   
		09 25627  rand > 6.4
		10 26092  rand > 7.2
		11 26557  rand > 8.0
		12 35540  rand > 8.9
		13 39362  rand > 36.8
		14 42391  rand > 41.8
		15 51548  rand > 57.2
		16 60705  rand > 62.2
		17 66432  rand > 85.7
		18 71851  rand > 89.3
		19 86975  rand > 92.9
		20 102100 rand > 96.5
		*/

	}
	
	
	
	
}




