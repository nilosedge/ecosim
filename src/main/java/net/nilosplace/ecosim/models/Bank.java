package net.nilosplace.ecosim.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.ToString;

@Data @ToString(callSuper = true)
public class Bank extends BaseEntity {

	private Map<String, Double> deposits = new HashMap<>();
	
	// Assets
	private Double cash = 0.0;
	// Money at call short term loans
	// Investments
	private List<Loan> assetLoans;
	
	// Liabilities
	// Capital from Shareholders
	private List<Loan> liabLoans; // Other banks or fed

	public void addDeposit(String personId, double amount) {
		if(!deposits.containsKey(personId)) {
			deposits.put(personId, amount);
			cash += amount;
		} else {
			deposits.put(personId, deposits.get(personId) + amount);
			cash += amount;
		}
	}
	
	
}
