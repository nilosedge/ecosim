package net.nilosplace.ecosim.models;

import lombok.Data;

@Data
public class Loan {
	private Double rate;
	private Double amount;
	private int nper;
}
