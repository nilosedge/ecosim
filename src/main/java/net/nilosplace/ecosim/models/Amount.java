package net.nilosplace.ecosim.models;

import lombok.Data;

@Data
public class Amount {
	protected Double amount;
}
