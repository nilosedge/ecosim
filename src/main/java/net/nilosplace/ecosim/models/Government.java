package net.nilosplace.ecosim.models;

import java.util.List;

import lombok.Data;

@Data
public class Government {
	private List<Bond> bonds;
}
