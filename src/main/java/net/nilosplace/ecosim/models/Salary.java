package net.nilosplace.ecosim.models;

import org.apache.commons.math3.distribution.NormalDistribution;

import lombok.Data;
import lombok.ToString;
import net.nilosplace.ecosim.App;

@Data @ToString(callSuper = true)
public class Salary extends Amount {
	public void setValue(Integer salary) {
		NormalDistribution dist = new NormalDistribution(salary, salary / 10);
		amount = Double.valueOf(dist.inverseCumulativeProbability(App.random.nextDouble()));
	}
}
