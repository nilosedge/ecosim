package net.nilosplace.ecosim.models;

import java.util.UUID;

import lombok.Data;

@Data
public class BaseEntity {
	public String id = UUID.randomUUID().toString();
}
