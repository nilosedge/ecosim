package net.nilosplace.ecosim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import net.nilosplace.ecosim.models.Bank;
import net.nilosplace.ecosim.models.Company;
import net.nilosplace.ecosim.models.Person;

public class App {
	
	public static Random random = new Random(1234);
	
	public static void main(String[] args) {

		List<Person> persons = new ArrayList<Person>();
		List<Bank> banks = new ArrayList<Bank>();
		List<Company> companies = new ArrayList<>();
		
		int amountOfBanks = 300;
		int amountOfCompanies = 600;
		int amountOfPersons = 45000;
		
		for(int i = 0; i < amountOfBanks; i++) {
			banks.add(new Bank());
		}

		int totalJobs = 0;
		for(int i = 0; i < amountOfCompanies; i++) {
			Company c = new Company(banks.get(App.random.nextInt(amountOfBanks)));
			companies.add(c);
			totalJobs += c.getMaxEmployees();
		}
		
		int unemployeed = 0;
		for(int i = 0; i < amountOfPersons; i++) {
			Person p = new Person(banks.get(App.random.nextInt(amountOfBanks)));
			persons.add(p);

			Collections.shuffle(companies);
			//System.out.println(list);
			//System.out.println(p.getSkillLevel());

			for(Company c: companies) {
				boolean added = c.addEmployee(p);
				if(added) break;
			}

			//if(list.size() == 0) {
			//	System.out.println(list);
			//}
			if(p.getCompany() == null) {
				unemployeed++;
			}
		}
		
		System.out.println("Total Jobs: " + totalJobs);
		System.out.println("unemployeed: " + unemployeed);


		for(Company c: companies) {
			System.out.println(c.getMaxEmployees() + " " + c.getMinSkillLevel() + " " + c.getMaxSkillLevel() + " " + c.getEmployees().size());
		}


	}
}