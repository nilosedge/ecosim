package net.nilosplace.ecosim.enums;

import java.util.Random;

public enum SpendingType {
	THRIFTY,
	CONSERVATIVE,
	NORMAL,
	AGGRESSIVE,
	GREEDY;
	
	public static SpendingType getRanomSpendingType() {
		return values()[(new Random().nextInt(SpendingType.values().length))];
	}
}
